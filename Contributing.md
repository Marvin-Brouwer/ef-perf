# Contribution guide  

Contributions are much appreciated.  
  
## Fixing a bug  

If you'd like to fix a bug from the issues list please assign it to yourself and create a branch with 'feature/{issueNr}'.  
If you want to fix a but that is not in the issue list, please creat an issue first and proceed with the above.  
  
When your change is done please create a merge request to the release branch the ticket is assigned to,   
If the change is valid you are requested to merge the branch yourself and close the issue once the merge is complete.  
  
## Branching model  

Because there is no real CI-Pipeline this is deminished to just creating branches and pull requests.  
  
## Adding a scenario  

This setup allows for extra scenario's to the tests. This is allowed as I'm done with the documentation now, you also don't have to update the screenshots or anything.  
Don't forget to update the [{demo}/src/constants/configuration.js][configuration-js] file.  
  

[//]: # (Labels)

[configuration-js]: https://gitlab.com/Marvin-Brouwer/ef-perf/blob/master/src/Demo/EFPerf.App/src/constants/configuration.js