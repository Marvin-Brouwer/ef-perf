[//]: # (Header)

<div align="center">
  <a  href="https://gitlab.com/Marvin-Brouwer/ef-perf#readme" 
      align="center" >
      <img  width="1024" height="288" 
            align="center" alt="Article banner" title="Entity Framework performance test" 
            src="https://gitlab.com/Marvin-Brouwer/ef-perf/raw/master/docs/banner.png?inline=false" />
  </a>
</div>
  
[![License][license-image]][license-url]  
  
[//]: # (Documentation)
  
## Entity framework performance test

Recently I've had a hunch that the way we are exposing imported data in some projects isn't the most efficient way.  
Just to be clear I hate SQL, I HATE I, HATE IT I HATE IT! Let's just say it's not my favorite type of programming language 😇.  
<div align="center">
  <img width="240" height="160" 
    align="center" alt="hate-gif" title="HATE!" 
    src="https://media.giphy.com/media/APbcWKvZf5ka7hU19j/giphy.gif" />
</div>  
However - to be fair - it works, and it has a lot of features that perform like a crazy and I think I should utilize this more.  
I was looking for a way to confirm that for myself (and possibly others) with a bit more visual representation than a table of averages,  
because of that I've created this project.  
  
The project is called EF perf because it originally only entailed Entity Framework, but I've added some additional scenario's for testing purposes.  
  
### How we usually do things

Generally, during the MVP phase of a project a bunch of stuff happens that creates technical debt[^1] usually ending up with non-content saved in a CMS somehow.  
The techlead leaves the project or company, someone else takes over and we refactor the stuff that doesn't belong in the CMS into data tables in SQL which we read using entity framework. And that's where it ends, this is usually a very big refactor and we leave it here meaning we end up with relational data we combine into viewmodels in memory.  
  
So why is this inefficient? Dotnet can handle this right?  
This might be kind of true, however this could mean your site becomes kind of slow which might increase over time.  
Also moving towards serverless architecture memory and CPU-ticks are becoming a valuable resource again and we need to think about this stuff.  
  
### The demo

To verify my hunch I'm creating a fake scenario that's structurally alike the [real world examples][real-world-examples] but no code is relatable to any real customer.  
_I chose SQL-Views over Stored Procedures because they give me an easy way to view the query result and it also caches stuff.  
In a real-life scenario, I would most-likely use Stored Procedures as they support paging._  
  
#### User story

Vincent is starting a cinema, The Vinema. Because he doesn't want to edit the movie data on his website himself he's using external parties for the content:
- Something like IMDB for the data like description, duration of the movie etc
- Something like Rotten tomatoes for the ratings,  
Keeping in mind this might use multiple sources in the future
- Something like an SAP platform to manage which movies are available at what point  
and which to promote

This data is imported to the database using several imports, the imports themselves are out of scope for this project but they are imported into three tables:  
- \[dbo].\[Movies]
- \[dbo].\[Promotions]
- \[dbo].\[Ratings]

![Database](https://gitlab.com/Marvin-Brouwer/ef-perf/raw/master/docs/Demo%20DB%20diagram.svg)  
  
For the demo I'm only focusing on the promotions part, so the availability is not a part of the database.
The Promoted movies API will expose all movies that apply to **all** these conditions:  
- They have a description
- They have a rating higher than or equal to 3
- Vincent has added them to the promotes movies list for a date range that applies to now
  
#### Test setup

![Azure setup](https://gitlab.com/Marvin-Brouwer/ef-perf/raw/master/docs/azure-setup.png)  
I've created an app that tests several implementations of an API combining the available movies into a list.  
The test shows a scatter plot of all test's elapsed time and a list of **average**, **peak** and **base** times.  
  
Because we work with the azure stack the test API's are written in azure functions.  
I also wanted to see the difference between local and hosted so I've setup a test project in my dev azure subscription.  
Because my subscription has a limit to what Azure offers for free the database is less performant. 
Because of this the test sample size is a lot smaller than locally, otherwise it would take ages to finish a single test.  
  
If it's still up, you can check out the hosted test <a href="https://efperfstorage.z6.web.core.windows.net" rel="nofollow">here</a> (please don't misuse this).  
In any other case I've recorded my results.  
You can toggle tests from the scatter plot by clicking the summary line, this will zoom in if possible.  
  
> Some tests may end up unavailable on the first try. This might mean the functions are still starting up, so you can just refresh.  
> If it keeps failing something else might be causing issues.
  
### Test results
[azure-test]: https://gitlab.com/Marvin-Brouwer/ef-perf/raw/master/docs/test-runs/azure.mp4
[local-test]: https://gitlab.com/Marvin-Brouwer/ef-perf/raw/master/docs/test-runs/local.mp4
[azure-full-run]: https://gitlab.com/Marvin-Brouwer/ef-perf/raw/master/docs/test-runs/azure-full-run.png
[local-full-run]: https://gitlab.com/Marvin-Brouwer/ef-perf/raw/master/docs/test-runs/local-full-run.png

**_Click the images for the corresponding test video_**

<table>
  <thead><tr>
    <th>Azure</th> <th>Local</th>
  </tr></thead>
  <tbody>
    <td>
      <a href="https://gitlab.com/Marvin-Brouwer/ef-perf/raw/master/docs/test-runs/azure.mp4" target="_blank" title="Click for Azure test video">
        <img src="https://gitlab.com/Marvin-Brouwer/ef-perf/raw/master/docs/test-runs/azure-full-run.png" alt="Still of test results" />
      </a>
    </td>
    <td>
      <a href="https://gitlab.com/Marvin-Brouwer/ef-perf/raw/master/docs/test-runs/local.mp4" target="_blank" title="Click for Local test video" >
        <img src="https://gitlab.com/Marvin-Brouwer/ef-perf/raw/master/docs/test-runs/local-full-run.png" alt="Still of test results" />
      </a>
    </td>
  </tr></tbody>
</table>
  
## Summary

So, after running some tests I can divide the results into three categories: **slow**, **meh** and **confetti**.
  
### Slow
[azure-slow]: https://gitlab.com/Marvin-Brouwer/ef-perf/raw/master/docs/test-runs/azure-slow.png
[local-slow]: https://gitlab.com/Marvin-Brouwer/ef-perf/raw/master/docs/test-runs/local-slow.png

| Azure                | Local                |
| -------------------- | -------------------- |
| ![Azure][azure-slow] | ![Local][local-slow] |  
  
So, with this dataset the average in memory way of combining data takes up about 2 to 3 seconds. That doesn't feel very fast to me.  
  
### Meh
[azure-meh]: https://gitlab.com/Marvin-Brouwer/ef-perf/raw/master/docs/test-runs/azure-meh.png
[local-meh]: https://gitlab.com/Marvin-Brouwer/ef-perf/raw/master/docs/test-runs/local-meh.png

| Azure               | Local               |
| ------------------- | ------------------- |
| ![Azure][azure-meh] | ![Local][local-meh] |  
  
The meh category just falls between the fast-performing stuff and the slow stuff, but I figured I'd include it anyway.  
I added the ORMLite using SQL-Views at the request of a colleague who referred ORMLite to me in the first place, he wanted to see this as a reference.  
  
### Confetti (fast)
[azure-confetti]: https://gitlab.com/Marvin-Brouwer/ef-perf/raw/master/docs/test-runs/azure-confetti.png
[local-confetti]: https://gitlab.com/Marvin-Brouwer/ef-perf/raw/master/docs/test-runs/local-confetti.png

| Azure                | Local                |
| -------------------- | -------------------- |
| ![Azure][azure-confetti] | ![Local][local-confetti] |  
  
This is where it gets interesting.  
I started out with the EF-Core using SQL-Views and the SqlDataAdapter because these would be the ones I would use depending on the project.  
I later added the SqlDataReader to see if using only async stuff would make a difference  
The thing that strikes the as odd here is that in azure the SqlDataReader seems to be slower than EF-Core even though it locally behaves as I expected.  
  
Anyways in conclusion I think it's safe to say it doesn't really matter whether you use Entity Framework or not, we should really take more time considering our data querying regardless of your ORM library if you're using one in the first place.  
  
## [Running locally][running-locally-url]
[running-locally-url]: https://gitlab.com/Marvin-Brouwer/ef-perf/blob/master/Runguide.md

See the [Runguide][running-locally-url] for help about running this project locally.  
  
## [Contributing][contributing-url]
[contributing-url]: https://gitlab.com/Marvin-Brouwer/ef-perf/blob/master/Contributing.md

See the [Contribution guide][contributing-url] for help about contributing to this project.  
  
## [Changelog][changelog-url]
[changelog-url]: https://gitlab.com/Marvin-Brouwer/ef-perf/blob/master/Changelog.md

See the [Changelog][changelog-url] to see the change history.  
  
# Footnotes

[^1]: DICLAIMER: I don't blame anyone for technical debt, stuff like that just happens with software development for a lot of different reasons.  
This is a separate topic altogether, in fact if you [Google](https://lmgtfy.com/?q=why+and+how+does+tecnical+debt+occur&s=) that you'll find a lot of different explanations, reasons and guidelines, as well as companies that claim to prevent technical debt.
  
[//]: # (Labels)

[license-url]: https://gitlab.com/Marvin-Brouwer/ef-perf/blob/master/License.md#blob-content-holder
[license-image]: https://img.shields.io/badge/license-Apache--2.0-blue.svg?style=flat-square