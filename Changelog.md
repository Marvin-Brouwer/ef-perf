# Changelog

## 1.5.0 (latest)
* Skipped a few versions because of random tweaking
* Created Vue app with scatter-plot

## 1.0.3
* Separate databases,
* Moved time measuring back to functions

## 1.0.2
* Disable api cache

## 1.0.1
* Using Test configuration in TS project
* Solution cleanup

## 1.0.0 (Initial version)
* Initial setup