﻿using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace EFPerf.Configuration
{
    public static class ConfigLoaderModule
    {
        public static void RegisterConfigurationRoot(this IWebJobsBuilder builder, string settingsName)
        {
            if (string.IsNullOrWhiteSpace(settingsName)) throw new ArgumentNullException(nameof(settingsName));

            builder.Services.AddSingleton(_ =>
            {
                var configurationBuilder = new ConfigurationBuilder()
                    .AddJsonFile(settingsName, optional: true, reloadOnChange: true)
                    .AddEnvironmentVariables();

                return configurationBuilder.Build();
            });
        }
    }
}
