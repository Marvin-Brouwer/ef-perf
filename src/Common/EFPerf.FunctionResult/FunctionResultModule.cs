﻿using EFPerf.Domain;
using EFPerf.TimingModelBuilder.Services;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.DependencyInjection;

namespace EFPerf.FunctionResult
{
    public static class FunctionResultModule
    {
        public static IWebJobsBuilder UseCustomResults(this IWebJobsBuilder builder)
        {
            builder.Services.AddTransient<IResultBuilder, TimedResultBuilder>();

            return builder;
        }
    }
}
