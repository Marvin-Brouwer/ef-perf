﻿using EFPerf.Domain;
using EFPerf.FunctionResult;
using EFPerf.TimingModelBuilder.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Diagnostics;

namespace EFPerf.TimingModelBuilder.Services
{
    public class TimedResultBuilder : IResultBuilder
    {
        private readonly DateTime _startTime;
        private readonly Stopwatch _stopWatch;

        public TimedResultBuilder()
        {
            _startTime = DateTime.UtcNow;
            _stopWatch = Stopwatch.StartNew();
        }

        public IActionResult CreateResult<TValue>(TValue data)
        {
            var endTime = DateTime.UtcNow;
            _stopWatch.Stop();

            var viewModel = new TimedViewModel<TValue>
            {
                Data = data,
                // Convert to miliseconds for javascript
                StartTime = ConverToJavascriptTicks(_startTime.Ticks),
                EndTime = ConverToJavascriptTicks(endTime.Ticks),
                ElapsedTime = ConverToJavascriptTicks(_stopWatch.ElapsedTicks)
            };

            return new UncachedOkObjectResult(viewModel);

            decimal ConverToJavascriptTicks(long ticks)
            {
                const decimal javaScriptUnPrecision = TimeSpan.TicksPerMillisecond;
                return ticks / javaScriptUnPrecision;
            }
        }
    }
}
