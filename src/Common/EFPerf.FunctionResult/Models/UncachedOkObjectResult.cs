﻿using Microsoft.AspNetCore.Mvc;

namespace EFPerf.FunctionResult
{
    public sealed class UncachedOkObjectResult : OkObjectResult
    {
        public UncachedOkObjectResult(object value) : base(value) { }

        public override void OnFormatting(ActionContext context)
        {
            base.OnFormatting(context);
            context.HttpContext.Response.Headers["Cache-Control"] = "max-age=0, no-cache, no-store, no-transform, must-revalidate";

        }
    }
}
