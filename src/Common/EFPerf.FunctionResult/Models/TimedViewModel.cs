﻿namespace EFPerf.TimingModelBuilder.Models
{
    public sealed class TimedViewModel<TDataModel>
    {
        public TDataModel Data { get; set; }
        public decimal StartTime { get; set; }
        public decimal EndTime { get; set; }
        public decimal ElapsedTime { get; set; }
    }
}
