﻿CREATE VIEW [dbo].[PromotedMovies] AS 

SELECT 
	[dbo].[CombinedMovies].[MovieName] as [Name],
	[dbo].[CombinedMovies].[Description],
	[dbo].[CombinedMovies].[Duration],
	[dbo].[CombinedMovies].[Score] as [Rating]

FROM [dbo].[CombinedMovies]
WHERE 
	[Description] is not null AND
	[PromotionStart] <= CURRENT_TIMESTAMP AND 
	[Score] >= 3 AND 
	(
		[PromotionEnd] is null OR 
		[PromotionEnd] > CURRENT_TIMESTAMP
	)

ORDER BY 
	[Name] ASC, 
	[Score] DESC, 
	[PromotionStart] ASC, 
	[PromotionEnd] ASC

OFFSET 0 ROWS