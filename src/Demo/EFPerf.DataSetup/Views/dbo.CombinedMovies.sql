﻿CREATE VIEW [dbo].[CombinedMovies] AS 

SELECT 
	[dbo].[Movies].[MovieName], 
	[dbo].[Movies].[Description], 
	[dbo].[Movies].[Duration], 

	[dbo].[Ratings].[Score],

	[dbo].[Promotions].[StartDate] as [PromotionStart], 
	[dbo].[Promotions].EndDate as [PromotionEnd]

FROM [dbo].[Movies]
INNER JOIN [dbo].[Promotions]
    ON [dbo].[Movies].[MovieName] = [dbo].[Promotions].[MovieName]
INNER JOIN [dbo].[Ratings]
    ON [dbo].[Movies].[MovieName] = [dbo].[Ratings].[MovieName]