const environment = process.env.NODE_ENV
// I know this is unsafe but it's just a demo
const selectEnvironmentSetting = (settings) => settings[environment]

export default {
  testAmount: selectEnvironmentSetting({
    development: 10,
    production: 5
  }),
  chunkSize: selectEnvironmentSetting({
    development: 50,
    production: 40
  }),
  graphSettings: {
    ytitle: 'Elapsed time (ms)',
    chartStyle: {
      fontFamily: 'monospace'
    }
  },
  tests: [
    {
      id: 'memoryEnityFramework',
      name: 'In memory EntityFramework-Core',
      color: '#0060ba',
      host: selectEnvironmentSetting({
        development: 'http://localhost:5860/api/PromotedMovies',
        production: 'https://ef-perf-memory-ef-core.azurewebsites.net/api/PromotedMovies'
      })
    },
    {
      id: 'memoryOrmLite',
      name: 'In memory ORMLite',
      color: '#d98a00',
      host: selectEnvironmentSetting({
        development: 'http://localhost:5865/api/PromotedMovies',
        production: 'https://ef-perf-memory-ss-ormlite.azurewebsites.net/api/PromotedMovies'
      })
    },
    {
      id: 'viewEntityFramework',
      name: 'Sql views with EntityFramework-Core',
      color: '#72a7fc',
      host: selectEnvironmentSetting({
        development: 'http://localhost:5861/api/PromotedMovies',
        production: 'https://ef-perf-views-ef-core.azurewebsites.net/api/PromotedMovies'
      })
    },
    {
      id: 'viewOrmLite',
      name: 'Sql views with ORMLite',
      color: '#fcc462',
      host: selectEnvironmentSetting({
        development: 'http://localhost:5862/api/PromotedMovies',
        production: 'https://ef-perf-views-ss-ormlite.azurewebsites.net/api/PromotedMovies'
      })
    },
    {
      id: 'jerryCurl',
      name: 'Jerry Curl sql builder',
      color: '#ff00d0',
      host: selectEnvironmentSetting({
        development: 'http://localhost:5870/api/PromotedMovies',
        production: null
      })
    },
    {
      id: 'viewSqlDataAdapter',
      name: 'Sql views with SqlDataAdapter',
      color: '#3acf5f',
      host: selectEnvironmentSetting({
        development: 'http://localhost:5863/api/PromotedMovies',
        production: 'https://ef-perf-views-sql-adapter.azurewebsites.net/api/PromotedMovies'
      })
    },
    {
      id: 'viewSqlDataReader',
      name: 'Sql views with SqlDataReader',
      color: '#fc6262',
      host: selectEnvironmentSetting({
        development: 'http://localhost:5864/api/PromotedMovies',
        production: 'https://ef-perf-views-sql-reader.azurewebsites.net/api/PromotedMovies'
      })
    }
  ]
}
