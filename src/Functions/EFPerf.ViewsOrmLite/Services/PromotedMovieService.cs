﻿using EFPerf.Domain.ViewModels;
using ServiceStack.OrmLite;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace EFPerf.ViewsOrmLite.Services
{
    public sealed class PromotedMovieService
    {
        private readonly DatabaseFactory _databaseFactory;

        public PromotedMovieService(DatabaseFactory databaseFactory)
        {
            _databaseFactory = databaseFactory;
        }

        public async Task<List<PromotedMovieViewModel>> ListPromotedMovies(CancellationToken token)
        {
            using var dbContext = await _databaseFactory.OpenAsync(token);

            var query = dbContext
                .From<PromotedMovieViewModel>();
            return await dbContext
                .SelectAsync(query, token)
                .ConfigureAwait(false);
        }
    }
}
