﻿using EFPerf.Configuration;
using EFPerf.FunctionResult;
using EFPerf.ViewsOrmLite;
using EFPerf.ViewsOrmLite.Services;
using EFPerf.ViewsORMLite;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Hosting;
using Microsoft.Extensions.DependencyInjection;

[assembly: WebJobsStartup(typeof(StartUp))]
namespace EFPerf.ViewsORMLite
{
    public class StartUp : IWebJobsStartup
    {
        public void Configure(IWebJobsBuilder builder)
        {
            builder.RegisterConfigurationRoot("local.settings.json");

            builder.AddBuiltInBindings();
            builder.AddExecutionContextBinding();
            builder.UseCustomResults();

            builder.Services.AddTransient<PromotedMovieService>();

            builder.Services.AddSingleton<DatabaseFactory>();
        }
    }
}
