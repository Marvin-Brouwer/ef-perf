﻿using EFPerf.Domain.ViewModels;
using Microsoft.Extensions.Configuration;
using ServiceStack.Data;
using ServiceStack.OrmLite;
using System.Data;
using System.Runtime.CompilerServices;
using System.Threading;

namespace EFPerf.ViewsOrmLite
{
    public sealed class DatabaseFactory
    {
        private readonly IDbConnectionFactory _dbFactory;
        public DatabaseFactory(IConfigurationRoot configuration)
        {
            var connectionString = configuration.GetConnectionStringOrSetting("EFPerf_Connectionstring");
            _dbFactory = new OrmLiteConnectionFactory(connectionString, SqlServerDialect.Provider);

            SetupAliases();
        }

        private void SetupAliases()
        {
            typeof(PromotedMovieViewModel).GetModelMetadata().Alias = "PromotedMovies";
        }

        public ConfiguredTaskAwaitable<IDbConnection> OpenAsync(CancellationToken token)
            => _dbFactory.OpenAsync(token).ConfigureAwait(false);
    }
}
