﻿using EFPerf.Configuration;
using EFPerf.FunctionResult;
using EFPerf.InMemoryOrmLite;
using EFPerf.InMemoryOrmLite.Services;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Hosting;
using Microsoft.Extensions.DependencyInjection;

[assembly: WebJobsStartup(typeof(StartUp))]
namespace EFPerf.InMemoryOrmLite
{
    public class StartUp : IWebJobsStartup
    {
        public void Configure(IWebJobsBuilder builder)
        {
            builder.RegisterConfigurationRoot("local.settings.json");

            builder.AddBuiltInBindings();
            builder.AddExecutionContextBinding();
            builder.UseCustomResults();

            builder.Services.AddTransient<MovieProvider>();
            builder.Services.AddTransient<PromotionProvider>();
            builder.Services.AddTransient<RatingProvider>();
            builder.Services.AddTransient<PromotedMovieService>();

            builder.Services.AddSingleton<DatabaseFactory>();
        }
    }
}
