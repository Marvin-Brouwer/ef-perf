using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using EFPerf.InMemoryOrmLite.Services;
using System.Threading.Tasks;
using EFPerf.Domain;
using System.Threading;

namespace EFPerf.InMemoryOrmLite.Functions
{
    public sealed class PromotedMovies
    {
        private readonly MovieProvider _movieProvider;
        private readonly PromotionProvider _promotionProvider;
        private readonly RatingProvider _ratingProvider;
        private readonly PromotedMovieService _promotedMovieService;
        private readonly IResultBuilder _resultBuilder;

        public PromotedMovies(MovieProvider movieProvider, PromotionProvider promotionProvider, RatingProvider ratingProvider, 
            PromotedMovieService promotedMovieService, IResultBuilder resultBuilder)
        {
            _movieProvider = movieProvider;
            _promotionProvider = promotionProvider;
            _ratingProvider = ratingProvider;
            _promotedMovieService = promotedMovieService;
            _resultBuilder = resultBuilder;
        }

        [FunctionName(nameof(GetPromotedMovies))] public async Task<IActionResult> GetPromotedMovies(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = nameof(PromotedMovies))]  HttpRequest req,
            ILogger log, CancellationToken token)
        {
            _ = req; // shutup compiler
            log.LogInformation($"Calling {nameof(GetPromotedMovies)} in {GetType().AssemblyQualifiedName}");

            var validMovieRecords = await _movieProvider.ListValidMovies(token);
            var validPromotions = await _promotionProvider.ListCurrentPromotions(token);
            var ratings = await _ratingProvider.ListPositiveRatings(token);

            var ratedPromotions = _promotedMovieService.FilterPromotableMovies(validMovieRecords, validPromotions, ratings);

            return _resultBuilder.CreateResult(ratedPromotions);
        }
    }
}
