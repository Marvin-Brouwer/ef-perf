﻿using EFPerf.Domain.Models;
using Microsoft.Extensions.Configuration;
using ServiceStack.Data;
using ServiceStack.OrmLite;
using System.Data;
using System.Runtime.CompilerServices;
using System.Threading;

namespace EFPerf.InMemoryOrmLite
{
    public sealed class DatabaseFactory
    {
        private readonly IDbConnectionFactory _dbFactory;
        public DatabaseFactory(IConfigurationRoot configuration)
        {
            var connectionString = configuration.GetConnectionStringOrSetting("EFPerf_Connectionstring");
            _dbFactory = new OrmLiteConnectionFactory(connectionString, SqlServerDialect.Provider);

            SetupAliases();
        }

        private void SetupAliases()
        {
            typeof(Movie).GetModelMetadata().Alias = "Movies";
            typeof(Promotion).GetModelMetadata().Alias = "Promotions";
            typeof(Rating).GetModelMetadata().Alias = "Ratings";
        }

        public ConfiguredTaskAwaitable<IDbConnection> OpenAsync(CancellationToken token)
            => _dbFactory.OpenAsync(token).ConfigureAwait(false);
    }
}
