﻿using EFPerf.Domain.Models;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace EFPerf.InMemoryOrmLite.Services
{
    public sealed class PromotionProvider
    {
        private readonly DatabaseFactory _databaseFactory;

        public PromotionProvider(DatabaseFactory databaseFactory) {
            _databaseFactory = databaseFactory;
        }

        public async Task<List<Promotion>> ListCurrentPromotions(CancellationToken token) 
        {
            var currentTime = DateTime.UtcNow;
            using var dbContext = await _databaseFactory.OpenAsync(token);

            var query = dbContext.From<Promotion>()
                .Where(promotion => promotion.StartDate <= currentTime)
                .Where(promotion => !promotion.EndDate.HasValue || promotion.EndDate < currentTime);
            return await dbContext
                .SelectAsync(query, token)
                .ConfigureAwait(false);
        }
    }
}