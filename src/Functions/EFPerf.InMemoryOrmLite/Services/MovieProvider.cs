﻿using EFPerf.Domain.Models;
using ServiceStack.OrmLite;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace EFPerf.InMemoryOrmLite.Services
{
    public sealed class MovieProvider
    {
        private readonly DatabaseFactory _databaseFactory;

        public MovieProvider(DatabaseFactory databaseFactory) {
            _databaseFactory = databaseFactory;
        }

        public async Task<List<Movie>> ListValidMovies(CancellationToken token) 
        {
            using var dbContext = await _databaseFactory.OpenAsync(token);

            var query = dbContext.From<Movie>()
                .Where(movie => movie.Description != null)
                .Where(movie => movie.Description != string.Empty);
            return await dbContext
                .SelectAsync(query, token)
                .ConfigureAwait(false);
        }
    }
}
