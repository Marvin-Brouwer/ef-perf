﻿using EFPerf.Domain.Models;
using EFPerf.Domain.ViewModels;
using System.Collections.Generic;
using System.Linq;

namespace EFPerf.InMemoryOrmLite.Services
{
    public sealed class PromotedMovieService
    {
        public List<PromotedMovieViewModel> FilterPromotableMovies(List<Movie> movies, List<Promotion> promotionDates, List<Rating> movieRatings)
        {
            var promotedMovies = GetPromotedMovies(movies, promotionDates).ToList();

            return GetRatedPromotions(promotedMovies, movieRatings)
                .OrderBy(movie => movie.Name)
                .ThenByDescending(movie => movie.Rating)
                .ToList();
        }

        private static IEnumerable<Movie> GetPromotedMovies(IEnumerable<Movie> validMovieRecords, IEnumerable<Promotion> validPromotions)
        {
            return validMovieRecords.Where(movie =>
                 validPromotions.Any(promotion => promotion.MovieName == movie.MovieName));
        }

        private static IEnumerable<PromotedMovieViewModel> GetRatedPromotions(IEnumerable<Movie> promotedMovies, IEnumerable<Rating> ratings)
        {
            foreach (var movie in promotedMovies)
            {
                var movieRating = ratings.FirstOrDefault(rating => rating.MovieName.Equals(movie.MovieName));
                if (movieRating?.Score.HasValue != true) continue;

                yield return new PromotedMovieViewModel
                {
                    Name = movie.MovieName,
                    Description = movie.Description,
                    Duration = movie.Duration,
                    Rating = movieRating.Score.Value
                };
            }
        }
    }
}
