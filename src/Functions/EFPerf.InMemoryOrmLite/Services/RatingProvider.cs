﻿using EFPerf.Domain.Models;
using ServiceStack.OrmLite;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace EFPerf.InMemoryOrmLite.Services
{
    public sealed class RatingProvider
    {
        private readonly DatabaseFactory _databaseFactory;

        public RatingProvider(DatabaseFactory databaseFactory) {
            _databaseFactory = databaseFactory;
        }

        public async Task<List<Rating>> ListPositiveRatings(CancellationToken token) 
        {
            using var dbContext = await _databaseFactory.OpenAsync(token);
            
            var query = dbContext.From<Rating>()
                .Where(rating => rating.Score.HasValue)
                .Where(rating => rating.Score >= 3);
            return await dbContext
                .SelectAsync(query, token)
                .ConfigureAwait(false);
        }
    }
}