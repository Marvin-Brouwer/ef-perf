﻿using EFPerf.Domain.ViewModels;
using Jerrycurl.Data.Queries;
using Jerrycurl.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace EFPerf.JerryCurl.Accessors
{
    public sealed class PromotedMoviesAccessor : Accessor
    {
        public PromotedMoviesAccessor(AccessorContext accessorContext)
        {
            Context = accessorContext;
        }

        private IProcResult ExecuteAndGetResult(string procName, object model, ProcArgs args)
        {
            IProcLocator procLocator = Context?.Locator ?? new ProcLocator();
            IProcEngine procEngine = Context?.Engine ?? new ProcEngine(null);
            PageDescriptor descriptor = procLocator.FindPage(procName, GetType());
            ProcFactory procFactory = procEngine.Proc(descriptor, args);
            return procFactory(model);
        }

        private string CheckGeneratedSql()
        {
            var procResult = ExecuteAndGetResult(nameof(ListPromotedMovies), null, new ProcArgs(
               typeof(PromotedMovieViewModel),
               typeof(IList<PromotedMovieViewModel>)));
            var queryOptions = GetQueryOptions(procResult.Domain);
            var queries = (procResult.Buffer as ISqlSerializer<QueryData>)?.Serialize(queryOptions);

            return queries.FirstOrDefault().QueryText;
        }

        //public Task<IList<PromotedMovieViewModel>> ListPromotedMovies(CancellationToken token)
        //{
        //    var query = CheckGeneratedSql();
        //    return QueryAsync<PromotedMovieViewModel>(null, null, nameof(ListPromotedMovies), token);
        //}

        public Task<IList<PromotedMovieViewModel>> ListPromotedMovies(CancellationToken token)
            => QueryAsync<PromotedMovieViewModel>(null, null, nameof(ListPromotedMovies), token);
    }
}
