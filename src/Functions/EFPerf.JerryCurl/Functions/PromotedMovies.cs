using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using EFPerf.Domain;
using EFPerf.Domain.ViewModels;
using EFPerf.JerryCurl.Accessors;
using System.Threading;
using System.Threading.Tasks;

namespace EFPerf.JerryCurl.Functions
{
    public sealed class PromotedMovies
    {
        private readonly IResultBuilder _resultBuilder;
        private readonly PromotedMoviesAccessor _promotedMoviesAccessor;

        public PromotedMovies(IResultBuilder resultBuilder, PromotedMoviesAccessor promotedMoviesAccessor)
        {
            _resultBuilder = resultBuilder;
            _promotedMoviesAccessor = promotedMoviesAccessor;
        }

        [FunctionName(nameof(GetPromotedMovies))] public async Task<IActionResult> GetPromotedMovies(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = nameof(PromotedMovies))]  HttpRequest req,
            ILogger log, CancellationToken token)
        {
            _ = req; // shutup compiler
            log.LogInformation($"Calling {nameof(GetPromotedMovies)} in {GetType().AssemblyQualifiedName}");

            var promotedMovies = await _promotedMoviesAccessor.ListPromotedMovies(token);

            return _resultBuilder.CreateResult<PromotedMovieViewModel>(null);
        }
    }
}
