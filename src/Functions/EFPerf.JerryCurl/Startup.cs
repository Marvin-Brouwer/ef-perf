﻿using EFPerf.Configuration;
using EFPerf.FunctionResult;
using EFPerf.JerryCurl;
using EFPerf.JerryCurl.Accessors;
using EFPerf.JerryCurl.Filters;
using Jerrycurl.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Hosting;
using Microsoft.Extensions.DependencyInjection;

[assembly: WebJobsStartup(typeof(StartUp))]
namespace EFPerf.JerryCurl
{
    public class StartUp : IWebJobsStartup
    {
        public void Configure(IWebJobsBuilder builder)
        {
            builder.RegisterConfigurationRoot("local.settings.json");

            builder.AddBuiltInBindings();
            builder.AddExecutionContextBinding();
            builder.UseCustomResults();

            builder.Services.AddTransient<PromotedMoviesAccessor>();
            
            builder.Services.AddSingleton<AccessorContext, AccessorContext>(sp => new AccessorContext(new ProcLocator(), new ProcEngine(sp)));
            builder.Services.AddTransient<JerryDomain>();
            builder.Services.AddTransient<ExceptionFilter>();
        }
    }
}
