﻿using EFPerf.JerryCurl.Filters;
using Jerrycurl.Mvc;
using Microsoft.Extensions.Configuration;
using System;

namespace EFPerf.JerryCurl
{
    public sealed class JerryDomain : IDomain
    {
        public void Configure(DomainOptions options)
        {
            var configuration = options.Services.GetService<IConfigurationRoot>();
            var connectionString = configuration.GetConnectionStringOrSetting("EFPerf_Connectionstring");
            options.UseSqlServer(connectionString);


            var isLocal = string.IsNullOrEmpty(Environment.GetEnvironmentVariable("WEBSITE_INSTANCE_ID"));

            AddFilters(options, isLocal);
        }

        private void AddFilters(DomainOptions options, bool isLocal)
        {
            if (isLocal) options.Sql.Filters.Add(new ExceptionFilter());
        }
    }
}
