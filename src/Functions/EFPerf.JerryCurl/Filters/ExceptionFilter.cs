﻿using Jerrycurl.Data.Filters;
using System.Data;

namespace EFPerf.JerryCurl.Filters
{
    internal sealed class ExceptionFilter : FilterHandler, IFilter
    {
        public IFilterAsyncHandler GetAsyncHandler(IDbConnection connection) => this;

        public IFilterHandler GetHandler(IDbConnection connection) => this;

        public override void OnException(FilterContext context)
        {
            context.Exception.Data.Add(nameof(context.Command.CommandText), context.Command.CommandText);
            base.OnException(context);
        }
    }

}
