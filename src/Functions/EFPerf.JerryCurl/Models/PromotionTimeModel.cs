﻿using System;

namespace EFPerf.JerryCurl.Models
{
    public sealed class PromotionTimeModel
    {
        public DateTime PromotionStart { get; set; }
        public DateTime? PromotionEnd { get; set; }
    }
}
