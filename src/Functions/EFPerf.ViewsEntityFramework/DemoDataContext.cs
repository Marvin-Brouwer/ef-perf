﻿using EFPerf.Domain.ViewModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;

namespace EFPerf.ViewsEntityFramework
{
    public sealed class DemoDataContext : DbContext
    {
        private readonly string _connectionString;

        public DemoDataContext(IConfigurationRoot configuration)
        {
            _connectionString = configuration.GetConnectionStringOrSetting("EFPerf_Connectionstring");
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(_connectionString, builder =>
                builder.EnableRetryOnFailure(5, TimeSpan.FromSeconds(10), null));
        }

        public DbSet<PromotedMovieViewModel> PromotedMovies { get; set; }
    }
}
