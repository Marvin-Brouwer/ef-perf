﻿using EFPerf.Configuration;
using EFPerf.FunctionResult;
using EFPerf.ViewsEntityFramework;
using EFPerf.ViewsEntityFramework.Services;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Hosting;
using Microsoft.Extensions.DependencyInjection;

[assembly: WebJobsStartup(typeof(StartUp))]
namespace EFPerf.ViewsEntityFramework
{
    public class StartUp : IWebJobsStartup
    {
        public void Configure(IWebJobsBuilder builder)
        {
            builder.RegisterConfigurationRoot("local.settings.json");

            builder.AddBuiltInBindings();
            builder.AddExecutionContextBinding();
            builder.UseCustomResults();

            builder.Services.AddTransient<PromotedMovieService>();

            builder.Services.AddDbContext<DemoDataContext>();
        }
    }
}
