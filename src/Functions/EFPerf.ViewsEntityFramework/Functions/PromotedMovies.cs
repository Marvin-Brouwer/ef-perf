using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using EFPerf.ViewsEntityFramework.Services;
using System.Threading.Tasks;
using EFPerf.Domain;
using System.Threading;

namespace EFPerf.ViewsEntityFramework.Functions
{
    public sealed class PromotedMovies
    {
        private readonly PromotedMovieService _promotedMovieService;
        private readonly IResultBuilder _resultBuilder;

        public PromotedMovies(PromotedMovieService promotedMovieService, IResultBuilder resultBuilder)
        {
            _promotedMovieService = promotedMovieService;
            _resultBuilder = resultBuilder;
        }

        [FunctionName(nameof(GetPromotedMovies))] public async Task<IActionResult> GetPromotedMovies(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = nameof(PromotedMovies))] HttpRequest req,
            ILogger log, CancellationToken token)
        {
            _ = req; // shutup compiler
            log.LogInformation($"Calling {nameof(GetPromotedMovies)} in {GetType().AssemblyQualifiedName}");

            var promotedMovies = await _promotedMovieService.ListPromotedMovies(token);

            return _resultBuilder.CreateResult(promotedMovies);
        }
    }
}
