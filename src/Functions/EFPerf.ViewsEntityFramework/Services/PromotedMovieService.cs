﻿using EFPerf.Domain.ViewModels;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading;

namespace EFPerf.ViewsEntityFramework.Services
{
    public sealed class PromotedMovieService
    {
        private readonly DemoDataContext _dbContext;

        public PromotedMovieService(DemoDataContext dbContext)
        {
            _dbContext = dbContext;
        }

        public ConfiguredTaskAwaitable<List<PromotedMovieViewModel>> ListPromotedMovies(CancellationToken token)
            => _dbContext.PromotedMovies
                .ToListAsync(token)
                .ConfigureAwait(false);
    }
}
