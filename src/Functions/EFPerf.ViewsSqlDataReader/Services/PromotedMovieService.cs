﻿using EFPerf.Domain.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading;
using System.Threading.Tasks;

namespace EFPerf.ViewsSqlDataReader.Services
{
    public sealed class PromotedMovieService
    {
        const string sqlCommandString = "SELECT * FROM [dbo].[PromotedMovies]";
        private readonly DatabaseFactory _databaseFactory;

        public PromotedMovieService(DatabaseFactory databaseFactory)
        {
            _databaseFactory = databaseFactory;
        }

        public async Task<List<PromotedMovieViewModel>> GetPromotedMovies(CancellationToken token)
        {
            using var connection = await _databaseFactory.OpenAsync(token);

            var sqlCommand = new SqlCommand(sqlCommandString, connection);
            using var sqlReader = await sqlCommand.ExecuteReaderAsync().ConfigureAwait(false);

            var promotedMoviesList = await MapRows(sqlReader);

            connection.Close();
            return promotedMoviesList;
        }

        private async Task<List<PromotedMovieViewModel>> MapRows(SqlDataReader sqlReader)
        {
            var promotedMoviesList = new List<PromotedMovieViewModel>();
            while (await sqlReader.ReadAsync().ConfigureAwait(false))
                promotedMoviesList.Add(MapRow(sqlReader));
            return promotedMoviesList;
        }

        private PromotedMovieViewModel MapRow(SqlDataReader dataRow)
            => new PromotedMovieViewModel
            {
                Name = (string)dataRow[nameof(PromotedMovieViewModel.Name)],
                Description = (string)dataRow[nameof(PromotedMovieViewModel.Description)],
                Duration = (TimeSpan)dataRow[nameof(PromotedMovieViewModel.Duration)],
                Rating = (short)dataRow[nameof(PromotedMovieViewModel.Rating)]
            };
    }
}
