﻿using Microsoft.Extensions.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Threading;
using System.Threading.Tasks;

namespace EFPerf.ViewsSqlDataReader
{
    public sealed class DatabaseFactory
    {
        private readonly string _connectionString;

        public DatabaseFactory(IConfigurationRoot configuration)
        {
            _connectionString = configuration.GetConnectionStringOrSetting("EFPerf_Connectionstring");
        }

        public async Task<SqlConnection> OpenAsync(CancellationToken token)
        {
            var _sqlConnection = new SqlConnection(_connectionString);
            if (_sqlConnection.State == ConnectionState.Closed)
                await _sqlConnection.OpenAsync(token).ConfigureAwait(false);

            return _sqlConnection;
        }
    }
}
