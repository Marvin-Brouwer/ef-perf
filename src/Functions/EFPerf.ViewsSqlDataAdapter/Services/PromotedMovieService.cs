﻿using System.Data;
using System.Data.SqlClient;
using System.Threading;
using System.Threading.Tasks;

namespace EFPerf.ViewsSqlDataAdapter.Services
{
    public sealed class PromotedMovieService
    {
        const string sqlCommand = "SELECT * FROM [dbo].[PromotedMovies]";
        private readonly DatabaseFactory _databaseFactory;

        public PromotedMovieService(DatabaseFactory databaseFactory)
        {
            _databaseFactory = databaseFactory;
        }

        public async Task<DataSet> GetPromotedMovies(CancellationToken token)
        {
            using var connection = await _databaseFactory.OpenAsync(token);

            var moviesAdapter = new SqlDataAdapter(sqlCommand, connection);
            var moviesDataSet = new DataSet();

            moviesAdapter.Fill(moviesDataSet, "PromotedMovies");

            connection.Close();
            return moviesDataSet;
        }
    }
}
