﻿using EFPerf.Domain.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;

namespace EFPerf.ViewsSqlDataAdapter.Services
{
    public sealed class MovieDataMapper
    {
        public List<PromotedMovieViewModel> MapData(DataSet moviesDataSet)
        {
            var rows = moviesDataSet.Tables["PromotedMovies"].Rows;
            var promotedMoviesList = new List<PromotedMovieViewModel>(rows.Count);

            foreach(DataRow dataRow in rows)
                promotedMoviesList.Add(MapRow(dataRow));

            return promotedMoviesList;
        }

        /// <summary>
        /// This is just so I can use yield
        /// </summary>
        /// <param name="moviesDataSet"></param>
        /// <returns></returns>
        private PromotedMovieViewModel MapRow(DataRow dataRow)
            => new PromotedMovieViewModel
            {
                Name = (string)dataRow[nameof(PromotedMovieViewModel.Name)],
                Description = (string)dataRow[nameof(PromotedMovieViewModel.Description)],
                Duration = (TimeSpan)dataRow[nameof(PromotedMovieViewModel.Duration)],
                Rating = (short)dataRow[nameof(PromotedMovieViewModel.Rating)]
            };
    }
}
