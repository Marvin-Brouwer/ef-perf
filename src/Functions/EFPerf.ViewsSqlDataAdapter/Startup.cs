﻿using EFPerf.Configuration;
using EFPerf.FunctionResult;
using EFPerf.ViewsSqlDataAdapter;
using EFPerf.ViewsSqlDataAdapter.Services;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Hosting;
using Microsoft.Extensions.DependencyInjection;

[assembly: WebJobsStartup(typeof(StartUp))]
namespace EFPerf.ViewsSqlDataAdapter
{
    public class StartUp : IWebJobsStartup
    {
        public void Configure(IWebJobsBuilder builder)
        {
            builder.RegisterConfigurationRoot("local.settings.json");

            builder.AddBuiltInBindings();
            builder.AddExecutionContextBinding();
            builder.UseCustomResults();

            builder.Services.AddTransient<PromotedMovieService>();
            builder.Services.AddTransient<MovieDataMapper>();

            builder.Services.AddSingleton<DatabaseFactory>();
        }
    }
}
