﻿using EFPerf.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;

namespace EFPerf.EFPerf.InMemoryEntityFramework
{
    public sealed class DemoDataContext : DbContext
    {
        private readonly string _connectionString;

        public DemoDataContext(IConfigurationRoot configuration) {
            _connectionString = configuration.GetConnectionStringOrSetting("EFPerf_Connectionstring");
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(_connectionString, builder =>
                builder.EnableRetryOnFailure(5, TimeSpan.FromSeconds(10), null));
        }

        public DbSet<Movie> Movies { get; set; }
        public DbSet<Promotion> Promotions { get; set; }
        public DbSet<Rating> Ratings { get; set; }
    }
}
