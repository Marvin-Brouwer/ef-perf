﻿using EFPerf.Domain.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;

namespace EFPerf.EFPerf.InMemoryEntityFramework.Services
{
    public sealed class RatingProvider
    {
        private readonly DemoDataContext _dbContext;

        public RatingProvider(DemoDataContext dbContext)
        {
            _dbContext = dbContext;
        }

        public ConfiguredTaskAwaitable<List<Rating>> ListPositiveRatings(CancellationToken token) 
        {
            return _dbContext.Ratings
                .Where(rating => rating.Score.HasValue)
                .Where(rating => rating.Score >= 3)
                .ToListAsync(token)
                .ConfigureAwait(false);
        }
    }
}