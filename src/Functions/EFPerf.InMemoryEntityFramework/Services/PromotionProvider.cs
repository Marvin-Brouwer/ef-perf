﻿using EFPerf.Domain.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;

namespace EFPerf.EFPerf.InMemoryEntityFramework.Services
{
    public sealed class PromotionProvider
    {
        private readonly DemoDataContext _dbContext;

        public PromotionProvider(DemoDataContext dbContext)
        {
            _dbContext = dbContext;
        }

        public ConfiguredTaskAwaitable<List<Promotion>> ListCurrentPromotions(CancellationToken token) 
        {
            var currentTime = DateTime.UtcNow;

            return _dbContext.Promotions
                .Where(promotion => promotion.StartDate <= currentTime)
                .Where(promotion => !promotion.EndDate.HasValue || promotion.EndDate > currentTime)
                .ToListAsync(token)
                .ConfigureAwait(false);
        }
    }
}