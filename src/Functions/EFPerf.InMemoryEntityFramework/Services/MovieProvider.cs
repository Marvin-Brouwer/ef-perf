﻿using EFPerf.Domain.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;

namespace EFPerf.EFPerf.InMemoryEntityFramework.Services
{
    public sealed class MovieProvider
    {
        private readonly DemoDataContext _dbContext;

        public MovieProvider(DemoDataContext dbContext) {
            _dbContext = dbContext;
        }

        public ConfiguredTaskAwaitable<List<Movie>> ListValidMovies(CancellationToken token) 
        {
            return _dbContext.Movies
                .Where(movie => !string.IsNullOrWhiteSpace(movie.Description))
                .ToListAsync(token)
                .ConfigureAwait(false);
        }
    }
}
