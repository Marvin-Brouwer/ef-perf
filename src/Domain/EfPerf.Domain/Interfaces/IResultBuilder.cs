﻿using Microsoft.AspNetCore.Mvc;

namespace EFPerf.Domain
{
    public interface IResultBuilder
    {
        IActionResult CreateResult<TDataModel>(TDataModel model);
    }
}
