﻿using Jerrycurl.Data.Metadata.Annotations;
using Jerrycurl.Mvc.Metadata.Annotations;
using System;

namespace EFPerf.Domain.Models
{
    [Table("dbo", "Movies")]
    public sealed class Movie
    {
        [Id, Key("PK_Movies", 1)]
        public int Id { get; set; }
        public string MovieName { get; set; }
        public string Description { get; set; }
        public TimeSpan Duration { get; set; }
    }
}
