﻿using Jerrycurl.Data.Metadata.Annotations;
using Jerrycurl.Mvc.Metadata.Annotations;
using System;

namespace EFPerf.Domain.Models
{
    [Table("dbo", "Promotions")]
    public sealed class Promotion
    {
        [Id, Key("PK_Promotions", 1)]
        public int Id { get; set; }
        public string MovieName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}
