﻿using Jerrycurl.Data.Metadata.Annotations;
using Jerrycurl.Mvc.Metadata.Annotations;

namespace EFPerf.Domain.Models
{
    [Table("dbo", "Ratings")]
    public sealed class Rating
    {
        [Id, Key("PK_Ratings", 1)]
        public int Id { get; set; }
        public string MovieName { get; set; }
        public short? Score { get; set; }
    }
}
