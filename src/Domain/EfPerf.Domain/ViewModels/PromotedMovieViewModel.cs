﻿using System;
using System.ComponentModel.DataAnnotations;

namespace EFPerf.Domain.ViewModels
{
    public class PromotedMovieViewModel
    {
        [Key] // <-- this is now neccesary for EF-Core
        public string Name { get; set; }
        public string Description { get; set; }
        public short Rating { get; set; }
        public TimeSpan Duration { get; set; }
    }
}
