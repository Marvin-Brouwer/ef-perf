# Runguide

This is a guide for setting up and running the project locally.  
  
## Prerequisites

You're expected to have the following installed:
- NodeJs v10.15.3 or higher
- A C# editing tool (preferably Visual Studio) that can run Azure functions
- Azure function runtime SDK
- DotNet-Core 3.0 SDK
- Access to a SQL Server instance preferably locally, the project connection refers to: "./"  
(SQL Server management studio is recommended)

  
## Installation
[publishing-db-png]: https://gitlab.com/Marvin-Brouwer/ef-perf/raw/master/docs/publishing-db.png

Open the _"~\src\Demo\EFPerf.App"_ in the terminal and run `npm install`.  
  
Open the .sln file, navigate to the {Solution}\Demo\EFPerf.DataSetup\Profiles\EFPerf.DataSetup.dev.publish.xml in the solution, right click Publish.  
![Publish][publishing-db-png]  
If your database is setup locally this should create everything automatically and add records to the database.  
  
To setup the solution just restore package or build solution.  
  
## Running the test suite

Because the way we mostly work is with a separate JS app running the website I've set up a separate VueJs app.  
At the company I work for while writing this we mostly use VueJs.  
  
To run the app open the _"~\src\Demo\EFPerf.App"_ in the terminal and run `npm start`.  
To start the API's simply hit F5 in Visual Studio or start a debugging session in your editor of choise,  
the solution is setup to start all the functions.